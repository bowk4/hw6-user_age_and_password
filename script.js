function createNewUser() {
    let firstName = prompt("Enter a your name:")
    let lastName = prompt("Enter a your surname:")
    let birthday = prompt("Your data birthday (у форматі dd.mm.yyyy):")

    const newUser = {
        _firstName: firstName,
        _lastName: lastName,
        birthday: birthday,
        getLogin: function () {
            return (this._firstName[0] + this._lastName).toLowerCase();
        },

        getAge: function () {
            const [day, month, year] = this.birthday.split(".");
            const birthDate = new Date(`${year}-${month}-${day}`);
            const today = new Date();
            // console.log(1)
            let age = today.getFullYear() - birthDate.getFullYear();
            const monthDiff = today.getMonth() - birthDate.getMonth();

            if (monthDiff < 0 || (monthDiff === 0 && today.getDate() < birthDate.getDate())) {
                age--;
            }
            return age;
        },
        getPassword: function () {
            const firstInitial = this._firstName[0].toUpperCase();
            const lastNameLower = this._lastName.toLowerCase();
            const birthYear = this.birthday.split(".")[2];
            return firstInitial + lastNameLower + birthYear;
        },


        setFirstName: function (newFirstName) {
            if (typeof newFirstName === 'string') {
                this._firstName = newFirstName;
            } else {
                console.log("Неприпустиме значення для імені.");
            }
        },
        setLastName: function (newLastName) {
            if (typeof newLastName === 'string') {
                this._lastName = newLastName;
            } else {
                console.log("Неприпустиме значення для прізвища.");
            }
        },
    };

    Object.defineProperty(newUser, 'firstName', {
        writable: false
    });
    Object.defineProperty(newUser, 'lastName', {
        writable: false
    });

    return newUser;
}

const user = createNewUser();
const login = user.getLogin();
console.log("Логін користувача:", login);

// user.setFirstName("Vasya");
// user.setLastName("Petrenko");
// console.log(user.getLogin());

const age = user.getAge();
console.log("Вік користувача:", age);

const password = user.getPassword();
console.log("Пароль:", password);


